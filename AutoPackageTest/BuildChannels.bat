@echo off
rem 输出目录
set OUT_DIR=d:\test
set build_path_abs=%cd%\%build_path_name%
echo 检查环境

if "%OUT_DIR%"=="" (
	echo 输出目录为空
	goto error
)

if not exist %OUT_DIR% (
	echo 输出目录不存在
	goto error
)
echo 检查环境完成,开始打包

for /f "tokens=1 eol=# delims= " %%i in (CHANNELS) do (
	echo ==================开始渠道号%%i打包=========================
	title %%i打包中
	call java -jar changeChannel.jar %%i %build_path_abs%
	call ant clean
	call ant release
	
	if "%time:~0,1%"==" " (
            move bin\AutoPackageTest-release.apk "%OUT_DIR%\AutoPackageTest_%%i_%date:~0,4%%date:~5,2%%date:~8,2%_%time:~1,1%%time:~3,2%.apk"
        ) else (
			move bin\AutoPackageTest-release.apk "%OUT_DIR%\AutoPackageTest_%%i_%date:~0,4%%date:~5,2%%date:~8,2%_%time:~0,2%%time:~3,2%.apk"
	)
	
	popd
	echo ==================渠道号%%i打包完成=========================
	title %%i打包完成
	
)

title 结束
echo 打包全部完成，请到%OUT_DIR%目录查看

echo ============================================================

goto end



:error

echo 有错误发生，打包终止

:end

pause>nul